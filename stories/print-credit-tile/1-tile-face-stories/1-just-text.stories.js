// eslint-disable-next-line no-unused-vars
import React from 'react';
import { storiesOf } from '@storybook/react';

// import { withKnobs } from '@storybook/addon-knobs';
import { addTileFaceStories } from "@ombiel/cm-tile-sdk/dev";

import PrintCreditTileFace from '../../../src/client/tiles/print-credit-tile/components/print-credit-tile-face';


const stories = storiesOf('Print Credit | Tile Face / Just Text', module);

addTileFaceStories(stories,<PrintCreditTileFace text="Print Credit" />);
