// eslint-disable-next-line no-unused-vars
import React from 'react';
import { storiesOf } from '@storybook/react';

// import { withKnobs } from '@storybook/addon-knobs';
import { addTileFaceStories,generateImage } from "@ombiel/cm-tile-sdk/dev";

import PrintCreditTileFace from '../../../src/client/tiles/print-credit-tile/components/print-credit-tile-face';

const image = generateImage("My Tile ","#556666");

const stories = storiesOf('Print Credit | Tile Face / With Image', module);

addTileFaceStories(stories,<PrintCreditTileFace image={image} text="Print Credit" />);
