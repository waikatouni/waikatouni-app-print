import React from "react";

import {
  Tile,
  TileBoard,
  // TileFace,
  HBlock,
  VBlock,
  TextBox,
  // TextLine,
  ImageBox,
  // Animation,
  // Badge,
  // Layer,
  // SvgBox,

  // useTileProps,
  // useCallback,
  // useCycle,
  // useFetch,
  // useMemo,
  // useReducer,
  // useRef,
  useServerAction,
  // useStash,
  // useState,
  // useTicker,
  // useTileConfig,
  // useTimedCycle,
  // useTimer,

} from "@ombiel/cm-tile-sdk";

export default function PrintCreditTile() {
  var title = "Unicash";
  var body = "loading...";
  var topImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAEHUlEQVR4nO2dzYocVRTH/0d0JBg3oi40gxndiqAuRHwCQ0AfYOJKJ4JGV3kIv4IfoARxY/II8QVcaEDjIhPcaaMiYnAMTibMmCx+LroGaqrL7uquvrdO1ZwfFExdTt9zpv517ld9SUEQBEEQBMHysVyOgBVJG5LWJT0p6b5cvhfklqRNSRclfW5mt3M4zSII8IikS5KezuEvAT9IOmlmf6R2lFyQIjMuq79i7HNF0vNmdielk7tSVl6wof6LIUnPSnq16yBaA1zmIJeAR7uOaxbAMeCrSuzfdB1Xa4CblX/qWNcxNQVYrcS+ndpnjj6EAw7Nso3slkHu+HP0IcEchCDOCEGccXduh9U2OThIZIgzQhBnhCDOyN6H9H0ekprIEGeEIM4IQZwRgjgjBHFGCOKMEMQZIYgzQhBnhCDOyL500kNu5HQW19SdEU2WM0IQZ4Qgzuhtpz6Ua/PVPjUyxBkhiDNCEGf0tg+ZwZaZPdh1EJIEbEl6oKl9ZEh65hp8DFWQL7sOoISnWMbD0zKp6gWuA1vAB8VjdC4AVoBzRWx/pjoe8wSURZAl1bkGvAdsAjvFtlmUrS3JRwjSsL63gb2azNtnFzjjLW43ASyzXuCtKUJUaSVKCDK7nseBf+cQZA84niruoY6yBBwBPgFuFNvHwJEa0zcklQcB25JOSToq6X5JrxRl+9wr6c0W/rqlqwwBPqs5uz+tsbtWsXmtxmajYrPZwl+S49GYDgXZqTlAE481A7cqNg/V2Dxcsdlp4W9q3INtsiTt1pTtNfjdopeYF/V3gCEL8lHDslFl/+Uam2rZzy38dUuHTZYBrwNfF9tpYOLsZzyzL/MPsA4cLbZTRVmZ91v4S3I8GtOVIHPUs8b0CWGVXeCxVHEPuclqhJmNJJ2d4ydnzeyXVPEkx3uGlOo7U5z90zJjYv7RddxuAkhRL3AceBe4yvgtRjeLv9+hxey84uNwCMJ4aftvxkvd3pbfPyxiu36YBClzbll+2lKI8b9U7Xt7b+8Mcf8ys4kZdxcw45p63JflnKFmSG+IDHFOCOKMEMQZvb1zcahPYkWGOCMEcUYI4oze9iG5AH4q75vZEyn99XZimIvc8UeT5YwQxBkhiDNCEGeEIM4IQZwRX0dwRmSIM0IQZ4QgzoivI8wgd58XGeKM3ggCnABGM26Qq2MEvNh1/E3pzWov8Kuk1QXD+N3MFvrCaKz2HnL6JMhpTT5+1oSRhvCV52UBbFfa9Pg48RRyZMiPlf3zfRAFWJV0vlJ8LbXfHPOQC5KeK+2fkPRbT5e0kr/7Kscoa0XSt5KeSe0rMd9JesHM7qR0krzJMrPbkk5KupLaV0K+l/RSajGkDBmyD3CPxqOddUlPafxyF8/sSLqqcZP7RQ4xgiAIgiAIglT8B10HbVqjgjHfAAAAAElFTkSuQmCC";

  const divider = [0, false];
  const boxProps = {
    backgroundColor: '#ffffff',
    backgroundOpacity: 0.3,
  };

  const [{ responseBody, error }] = useServerAction("print", {
    method: "GET",
    isJsonResponse: true,
    updateRate: 2000,
  });

  console.log('Print Credit Error:', error);

  if (responseBody) {
    if (responseBody.credit === "You have unlimited credit") {
      body = 'Unlimited Print Credit';
    } else if (responseBody === 'undefined') {
      body = 'Credit Not Found';
    } else {
      body = `$${responseBody.total}`;
    }
  }

  return (
    <Tile>
      <TileBoard>
        <HBlock divider={divider}>
          <VBlock backgroundColor="#F2F2F2" divider={divider} {...boxProps}>
            <HBlock padding={[0, 5]} backgroundColor="#F2F2F2">
              <TextBox flex={4} backgroundColor="#F2F2F2" color="#333333" textAlign="center" bold>{title}</TextBox>
            </HBlock>
            <TextBox backgroundColor="#F2F2F2" color="#353535" textAlign="center">{body}</TextBox>
            <HBlock onClick="campusm://openURL?url=https%3A%2F%2Fkm-monitor.its.waikato.ac.nz%2Fmymonitor" backgroundColor="#353535">
              <ImageBox flex={1} src={topImage} imageFit="contain" backgroundColor="#353535" />
              <TextBox flex={3} backgroundColor="#353535" color="#FFFFFF" bold textAlign="left">TOP UP ACCOUNT</TextBox>
            </HBlock>
          </VBlock>
        </HBlock>
      </TileBoard>
    </Tile>
  );
}
