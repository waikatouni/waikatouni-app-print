import {registerTileComponent} from "@ombiel/cm-tile-sdk";
import PrintCreditTile from "./tiles/print-credit-tile/components/print-credit-tile";

registerTileComponent(PrintCreditTile,"PrintCreditTile");
